using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
public class ScareSensor : MonoBehaviour
{
    [SerializeField]
    string tagToSense = "scareTag";
    [SerializeField]
    bool doCountdown;
    [SerializeField]
    private int countDownTime = 1;
    bool _doingAction = false;
    [SerializeField]
    UnityEvent _MyScareEvent;
    [SerializeField]
    UnityEvent _MyScareEndEvent;
    [SerializeField]
    string pickedItemToSense = "itemTag";
    public bool doGettable;
    public PickableItem pickedItem;
    public PickableItem gettableItem;
    [SerializeField]
    UnityEvent _MyPickedItemEvent;

    private void OnTriggerEnter(Collider other)
	{
        if (_doingAction) return;
        if (other.CompareTag(tagToSense))
        {
            Debug.Log("Hit by scareTag object");
            _MyScareEvent.Invoke();

            if (doCountdown) StartCountDown();
        }
        if (!doGettable) return;
        Debug.Log("checking gettable");
        // Check if object is gettable
        gettableItem = other.transform.parent.GetComponent<PickableItem>();
        Debug.Log(gettableItem + " = gettable");
        if (gettableItem)
        {
            Debug.Log("item is gettable");
            // Pick it
            GotItem(gettableItem);
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (_doingAction) return;
        if (other.CompareTag(tagToSense))
        {
            Debug.Log("scareTag object left");
            
        }
    }

    public void StartCountDown()
    {
        _doingAction = true;
        StartCoroutine(DoCountDown());

    }

    IEnumerator DoCountDown()
    {

        yield return new WaitForSeconds(countDownTime);
        _MyScareEndEvent.Invoke();
        _doingAction = false;
    }

    private void GotItem(PickableItem item)
    {
        if (item.itemName == pickedItemToSense)
        {
            // Assign reference
            pickedItem = item;
            // Disable rigidbody and reset velocities
            item.Rb.isKinematic = true;
            item.Rb.velocity = Vector3.zero;
            item.Rb.angularVelocity = Vector3.zero;
            // Set this as a parent
            item.transform.SetParent(transform);
            // Reset position and rotation
            item.transform.localPosition = Vector3.zero;
            item.transform.localEulerAngles = Vector3.zero;
            _MyPickedItemEvent.Invoke();
        }
    }
}
