using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class FadeInAndOut : MonoBehaviour
{
    [SerializeField] Color startColor;
    [SerializeField] Color endColor;
    public Color currentColor;
    Color materialStoredColor;
    
    [SerializeField] Image currentImage;
    [SerializeField] private TextMeshProUGUI textToUse;
    [SerializeField] Material currentMaterial;
    bool _doingFade;
    [SerializeField] float fadeTime = 1f;
    [SerializeField] bool _fadeOnStart;

    // Start is called before the first frame update
    void Start()
    {
        if (currentMaterial) materialStoredColor = currentMaterial.color;
    }

	private void OnEnable()
	{
        if (_fadeOnStart) DoFadeOut();
    }
	private void OnDisable()
	{
        if (currentMaterial) currentMaterial.color = materialStoredColor;

    }

	// Update is called once per frame
	void Update()
    {
        
    }

    public void DoFadeIn()
    {
        if (_doingFade) return;
        _doingFade = true;
        StartCoroutine(DoAThingOverTime(startColor, endColor, fadeTime));
    }

    public void DoFadeOut()
    {
        if (_doingFade) return;
        _doingFade = true;
        StartCoroutine(DoAThingOverTime(endColor, startColor, fadeTime));
    }


    IEnumerator DoAThingOverTime(Color start, Color end, float duration)
    {
        for (float t = 0f; t < duration; t += Time.deltaTime)
		{
			float normalizedTime = t / duration;
			//right here, you can now use normalizedTime as the third parameter in any Lerp from start to end
			currentColor = Color.Lerp(start, end, normalizedTime);

			SetTheColors();

			yield return null;
		}
		currentColor = end; //without this, the value will end at something like 0.9992367
        SetTheColors();

        _doingFade = false;
    }

	private void SetTheColors()
	{
		if (currentImage)
			currentImage.color = currentColor;
		if (textToUse)
			textToUse.color = currentColor;
		if (currentMaterial)
			currentMaterial.color = currentColor;
	}
}
