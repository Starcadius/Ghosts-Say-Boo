using System.Collections;
using UnityEngine;
using UnityEngine.Events;
//#if ENABLE_INPUT_SYSTEM
using UnityEngine.InputSystem;
//#endif
namespace StarcadiusStuff
{
    public class SimpleGrabSystem : MonoBehaviour
    {
#if ENABLE_INPUT_SYSTEM
        [SerializeField]
        private PlayerInput _playerInput;
#endif

        [SerializeField]
        private PlayerAssetsInputs _input;

        // Reference to the character camera.
        [SerializeField]
        private Camera characterCamera;
        // Reference to the slot for holding picked item.
        [SerializeField]
        private Transform slot;
        // Reference to the currently held item.
        public PickableItem pickedItem;
        public Transform pickedItemtransform;
       // public float smoothSpeed = 1f;
        public float smoothTime = 1f;
        private Vector3 velocity = Vector3.zero;
        private Transform tempObject;
        private void Start()
		{
            if (!_input)
                _input = GetComponent<PlayerAssetsInputs>();
#if ENABLE_INPUT_SYSTEM
            if(!_playerInput)
            _playerInput = GetComponent<PlayerInput>();
#else
			Debug.LogError( "PlayerInput not accessable, are you set with new input system?");
#endif
        }

		private void OnTriggerEnter(Collider other)
		{
            if (other.transform.parent.GetComponent<PickableItem>())
            {
                tempObject = other.transform;
            }
		}
		private void Update()
        {
            if (_input.stopInteract)
            {
                _input.stopInteract = false;
                // Check if player picked some item already
                if (pickedItem)
                {
                    // If yes, drop picked item
                    DropItem(pickedItem);
                }
            }

            // Execute logic only on button pressed
            if (_input.interact)
            {
                _input.interact = false;
                // Check if player picked some item already
                if (pickedItem)
                {
                    // If yes, drop picked item
                    DropItem(pickedItem);
                }
                else
                {
                    // If no, try to pick item in front of the player
                    // Create ray from center of the screen
                    var ray = characterCamera.ViewportPointToRay(Vector3.one * 0.5f);
                    RaycastHit hit;
                    // Shot ray to find object to pick
                    if (Physics.Raycast(ray, out hit, 3f))
                    {
                        // Check if object is pickable
                        var pickable = hit.transform.GetComponent<PickableItem>();
                        // If object has PickableItem class
                        if (pickable)
                        {
                            // Pick it
                            PickItem(pickable);
                        }
                    }
                }
            }
        }

		private void FixedUpdate()
		{
      
            if (pickedItemtransform != null)
            {
                float dist = Vector3.Distance(pickedItemtransform.position, slot.position);
                if (dist <= 0.1f)
                {
                    // Set Slot as a parent
                    pickedItemtransform.transform.SetParent(slot);
                    // Reset position and rotation
                    pickedItemtransform.transform.localPosition = Vector3.zero;
                    pickedItemtransform.transform.localEulerAngles = Vector3.zero;
					pickedItemtransform = null;
                }

                pickedItemtransform.position = Vector3.SmoothDamp(pickedItemtransform.position, slot.position, ref velocity, smoothTime);
            }
		}

		/// <summary>
		/// Method for picking up item.
		/// </summary>
		/// <param name="item">Item.</param>
		private void PickItem(PickableItem item)
        {
            // Assign reference
            pickedItem = item;
            // Disable rigidbody and reset velocities
            item.Rb.isKinematic = true;
            item.Rb.velocity = Vector3.zero;
            item.Rb.angularVelocity = Vector3.zero;
            // Set Slot as a parent
            //item.transform.SetParent(slot);
            pickedItemtransform = item.transform;
            // Reset position and rotation
            //item.transform.localPosition = Vector3.zero;
            //item.transform.localEulerAngles = Vector3.zero;
        }
        /// <summary>
        /// Method for dropping item.
        /// </summary>
        /// <param name="item">Item.</param>
        private void DropItem(PickableItem item)
        {
            // Remove reference
            pickedItem = null;
            pickedItemtransform = null;
            // Remove parent
            item.transform.SetParent(null);
            // Enable rigidbody
            item.Rb.isKinematic = false;
            // Add force to throw item a little bit
            item.Rb.AddForce(item.transform.forward * 2, ForceMode.VelocityChange);
        }
    }
}
