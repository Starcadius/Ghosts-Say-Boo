using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemSpawner : MonoBehaviour
{
    public Transform[] locations;
    public GameObject[] items;
    public GameObject tempItem;
    

    public GameObject AddObject;
    private Transform tempGO;
    // Start is called before the first frame update
    void Start()
    {
        Shuffle();
     
        for (int i = 0; i < items.Length; i++)
        {
            Debug.Log("i ="+i);
            Instantiate(items[i].gameObject, locations[i].position, Quaternion.identity);

        }
    }

    public void Shuffle()
    {
        for (int i = 0; i < locations.Length - 1; i++)
        {
            int rnd = Random.Range(i, locations.Length);
            tempGO = locations[rnd];
            locations[rnd] = locations[i];
            locations[i] = tempGO;
        }
    }


}
