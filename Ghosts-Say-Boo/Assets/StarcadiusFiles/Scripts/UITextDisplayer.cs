using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class UITextDisplayer : MonoBehaviour
{
    [SerializeField] public TextMeshProUGUI textToUse;
    //public float floatDisplay;
    public PlayerRecharge playerRecharge;
    // Start is called before the first frame update
    void Start()
    {
        playerRecharge = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerRecharge>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void UpdateMyFloatText(float floatText)
    {
        textToUse.text = floatText.ToString();
    }
}
