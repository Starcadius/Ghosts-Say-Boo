using System.Collections;
using UnityEngine;
using UnityEngine.Events;
#if ENABLE_INPUT_SYSTEM
using UnityEngine.InputSystem;
#endif


namespace StarcadiusStuff
{
#if ENABLE_INPUT_SYSTEM
    [RequireComponent(typeof(PlayerInput))]
#endif

    public class PlayerAttack : MonoBehaviour
    {
        [SerializeField]
        private GameObject scareAttackObject;

        [SerializeField]
        Transform shootRoot;

        [SerializeField]
        private int resetCountDown = 1;

        [SerializeField]
        bool _doingScare;

        [SerializeField]
        UnityEvent _doAction;

        [SerializeField]
        bool _isCharged;

#if ENABLE_INPUT_SYSTEM
        private PlayerInput _playerInput;
#endif

       // private CharacterController _controller;
        private PlayerAssetsInputs _input;

        // Start is called before the first frame update
        void Start()
        {
            if (shootRoot == null) shootRoot = this.transform;
           // _controller = GetComponent<CharacterController>();
            _input = GetComponent<PlayerAssetsInputs>();
#if ENABLE_INPUT_SYSTEM
            _playerInput = GetComponent<PlayerInput>();
#else
			Debug.LogError( "PlayerInput not accessable, are you set with new input system?");
#endif

        }

        // Update is called once per frame
        void Update()
        {
            if (_doingScare||!_isCharged) return;
            Scare();
        }

        private void Scare()
        {
            if (_input.shoot)
            {
                _doingScare = true;
                StartCoroutine(DoCountDown());
                _input.shoot = false;
                _doAction.Invoke();
                Instantiate(scareAttackObject, shootRoot.position, shootRoot.rotation);

            }
        }

        public void IsCharged()
        {
            _isCharged = true;
        }

        public void IsDepleted()
        {
            _isCharged = false;
        }

        IEnumerator DoCountDown()
        {

            yield return new WaitForSeconds(resetCountDown);
            _doingScare = false;
        }
    }
}
