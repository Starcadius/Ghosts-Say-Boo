using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

[System.Serializable]
public class MyFloatEvent : UnityEvent<float>
{
}
public class PlayerRecharge : MonoBehaviour
{
    public bool isAlwaysCharged;
    public bool _inEnergyZone;
    bool onCharger;
    bool isCharged;
    public int timeToRecharge = 5;
    float fullCharge = 5;
    public float chargeAmount;
    public float depletionMultiplier = 0.5f;

    public MyFloatEvent timerEvent;
    public UnityEvent doAction;
    public UnityEvent playerCharged;
    public UnityEvent playerDepleted;
    // Start is called before the first frame update
    void Start()
    {
        fullCharge = timeToRecharge;
        timerEvent.Invoke(chargeAmount);
    }

    // Update is called once per frame
    void Update()
    {
        if (isAlwaysCharged)
        {
            if (!isCharged)
            {
                isCharged = true;
                playerCharged.Invoke();
            }
        }
       
        if (onCharger)
        {
            if (chargeAmount < fullCharge)
            {
                chargeAmount += Time.deltaTime;
                timerEvent.Invoke(chargeAmount);
            }
            else
            {
                if (!isCharged)
                {
                    isCharged = true;
                    playerCharged.Invoke();
                }
            }
        }
        else
        {
            if (isAlwaysCharged) return;
            if (chargeAmount > 0)
            {
                chargeAmount -= Time.deltaTime * depletionMultiplier;
                timerEvent.Invoke(chargeAmount);
            }
            else
            {
                isCharged = false;
                playerDepleted.Invoke();
            }
        }
    }

	private void OnTriggerEnter(Collider other)
	{
        if (other.CompareTag("FreshEnergy"))
        {
            _inEnergyZone = true; ;
            Debug.Log("in Energy Zone;");

        }
    }

    private void OnTriggerStay(Collider other)
    {
        if (other.CompareTag("FreshEnergy"))
        {
            if (isCharged) return;
            if (onCharger) return;
            chargeAmount = 0;
            onCharger = true;
            
            Debug.Log("charging;");

        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("FreshEnergy"))
        {
            _inEnergyZone = false; ;
            Debug.Log("away from Energy Zone;");
            onCharger = false;
        }
    }

   
}
