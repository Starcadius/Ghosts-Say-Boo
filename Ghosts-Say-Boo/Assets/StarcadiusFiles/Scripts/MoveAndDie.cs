using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveAndDie : MonoBehaviour
{
    [SerializeField]
    private float speed = 5.0f;

 

    // Update is called once per frame
    void Update()
    {
        transform.Translate(Vector3.forward * speed * Time.deltaTime);
    }
    
    
	private void OnTriggerEnter(Collider other)
    {
        DestroyMe();

    }

    public void DestroyMe()
    {
        Destroy(this.gameObject);
    }
}
