using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;


//public class MyFloatEvent : UnityEvent<float>
//{
//}
public class NPCFeelings : MonoBehaviour
{
    public float scareTimer;
    
    bool isScared;

    [SerializeField]
    UnityEvent _MyFeelingEvent;
    public MyFloatEvent timerEvent;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (!isScared) return;
        if (scareTimer > 0)
        {
            scareTimer -= Time.deltaTime;
            timerEvent.Invoke(scareTimer);
        }
        else
        {
            isScared = false;
        }
    }

    public void StartScare()
    {
        isScared = true;
    }
}
