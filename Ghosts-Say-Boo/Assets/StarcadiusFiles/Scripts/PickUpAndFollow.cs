using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PickUpAndFollow : MonoBehaviour
{
    public bool isPickedUp;
    [SerializeField]
    Transform followObject;

    // Start is called before the first frame update
    void Start()
    {
        //find the pickup ref object at beginning
        followObject = GameObject.FindGameObjectWithTag("playerFollowObject").transform;
        
    }

    // Update is called once per frame
    void Update()
    {
        if (isPickedUp)
        {
            //float to camera height
            //follow player, stay in front of camera
        }
    }

    public void PickUp()
    {
        isPickedUp = true;
    }

    public void Drop()
    {
        isPickedUp = false;
    }
}
