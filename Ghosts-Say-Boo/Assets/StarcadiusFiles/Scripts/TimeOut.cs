using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
public class TimeOut : MonoBehaviour
{
   
    [SerializeField]
    private int countDownTime = 1;
    [SerializeField]
    UnityEvent _doAction;
    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(DoCountDown());
    }

   


    IEnumerator DoCountDown()
    {
       
        yield return new WaitForSeconds(countDownTime);
        _doAction.Invoke();
    }

    public void DestroyMe()
    {
        Destroy(this.gameObject);
    }
}
